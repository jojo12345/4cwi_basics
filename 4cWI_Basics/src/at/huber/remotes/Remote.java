package at.huber.remotes;

import java.util.ArrayList;
import java.util.List;

//private: es ist nur innerhalb der Klasse sichtbar bzw. zugreifbar
//Klasse
public class Remote {
	//Fields
	private String serialNumber;
	private String width;
	public String length;
	private boolean isOn;
	//Eine Liste mit theoretisch unlimitierter Kapazit�t, in der der Typ devices (in diesem Fall eine Klasse) abgespeichert wird
	private List<Device> devices;
	

	//Constructor: gibt fields(=Variablen) Werte
	//k�nnen automatisch �ber das Men� "Source" ausgew�hlt werden
	//Allen fields denen eine Wert gegeben werden soll m�ssen in den Parametern (=runde Klammern)
	public Remote(String width, String length, String serialNumber) {
		//this ist der Verweis auf diese Instanz der Klasse
		this.width = width;
		this.length = length;
		this.serialNumber = serialNumber;
		this.isOn = true;
		//Erzeugt eine neue Liste, auf die this.devices zeigt.
		this.devices = new ArrayList<>();
	}
	
	//Diese Funktion gibt nichts zur�ck und ben�tigt die Variable device vom Typ Device (eine Klasse).
	public void addDevice(Device device) {
		//F�gt der Liste devices einen neuen Eintrag hinzu.
		this.devices.add(device);
	}

	
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	//getter
	public String getWidth() {
		return width;
	}

	//setter
	public void setWidth(String width) {
		this.width = width;
	}

	
	//getter
	public String getlength() {
		return length;
	}
	
	//setter
	public void setlength(String length) {
		this.length = length;
	}
	
	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	

}
