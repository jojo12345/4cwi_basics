package at.huber.remotes;

public class RemoteMain {
	public static void main(String[] args) {
		
		//Remote ist eine von uns erzeugte Klasse, die beschreibt, was f�r Eigenschaften
		//Erzeugt Instanz der Klasse Remote, die Variable r1 verweist darauf.
		// Alles in den runden Klammern wird dem Constructor �bergeben.
		Device d1 = new Device("fdssd4545", Device.type.television);
		Remote r1 = new Remote("10", "15", "4561dfs");
		//Einer Remote eine neue Device zuweisen.
		r1.addDevice(d1);
		
		//for ([name des Wertes, der aus der Liste genommen wird] [Datentyp der in der Liste gespeicherten Werte] : [Liste])
		for (Device device : r1.getDevices()) {
			//Geordnet: So wie es in der Liste steht. Verwenden wir hier
			//Sortiert: Nach z.B. Alphabet sortiert.
			System.out.println(device.getSerialNumber());
		}

		
		
		Remote r2 = new Remote("20", "25", "487561sdf");
		//r3 verweist auf dasselbe Objekt (eine Instanz der Klasse Remote)
		Remote r3 = r2;

	}
}
