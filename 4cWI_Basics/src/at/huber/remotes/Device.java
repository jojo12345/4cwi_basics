package at.huber.remotes;

public class Device {
	private String serialNumber;
	//Eine Variable mit dem Namen deviceType vom Typ type.
	private type deviceType;
	
	//In einer Enumeration kann man nur die hier spezifizierten Werte eingeben.
	//Weil die Variable deviceType vom Typ type ist, kann man in ihr nur die hier aufgelisteten Werte eingeben.
	public enum type {
		television, beamer, computer
	}

	public Device(String serialNumber, type deviceType) {
		super();
		this.serialNumber = serialNumber;
		this.deviceType = deviceType;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public type getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}
	
	

}
