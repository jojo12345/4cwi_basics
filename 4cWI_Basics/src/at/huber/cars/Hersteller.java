package at.huber.cars;
public class Hersteller {
	private String name;
	private String Herkunftsland;
	private int rabatt;

	public Hersteller(	String name, String Herkunftsland, int rabatt) {
		this.name = "VW";
		this.Herkunftsland = "Österreich";
		this.rabatt = 5;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHerkunftsland() {
		return Herkunftsland;
	}

	public void setHerkunftsland(String herkunftsland) {
		Herkunftsland = herkunftsland;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}

}
