package at.huber.cars;

public class Car {
	private String farbe;
	private String maximalgeschwindigkeit;
	private int basispreis;
	private int basisverbrauch;
	private int mileage;
	private int correctedComsumption;
	private Motor motor;
	private Hersteller hersteller;

	public Car(String farbe, String maximalgeschwindigkeit, int basispreis, int basisverbrauch, Motor motor,
			Hersteller hersteller, int mileage) {
		this.farbe = farbe;
		this.maximalgeschwindigkeit = maximalgeschwindigkeit;
		this.basispreis = basispreis;
		this.basisverbrauch = basisverbrauch;
		this.motor = motor;
		this.hersteller = hersteller;
		this.mileage = mileage;

	}

	public int getCorrectedComsumption() {
		int returnValue = 0;
		
		if (this.mileage>50000) {
			returnValue = (int) ((double) 0.12 * this.basisverbrauch);
		}else {
			returnValue = this.basispreis;
		}
		
		return returnValue;
	}

	public void setCorrectedComsumption(int correctedComsumption) {
		this.correctedComsumption = correctedComsumption;
	}

	public int getBasisverbrauch() {
		return basisverbrauch;
	}

	public void setBasisverbrauch(int basisverbrauch) {
		this.basisverbrauch = basisverbrauch;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getMaximalgeschwindigkeit() {
		return maximalgeschwindigkeit;
	}

	public void setMaximalgeschwindigkeit(String maximalgeschwindigkeit) {
		this.maximalgeschwindigkeit = maximalgeschwindigkeit;
	}

	public int getPrice() {
		return (int) (this.basispreis * (1 - (double) this.hersteller.getRabatt() / 100));
	}

	public int getBasispreis() {
		return basispreis;
	}

	public void setBasispreis(int basispreis) {
		this.basispreis = basispreis;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Hersteller getHersteller() {
		return hersteller;
	}

	public void setHersteller(Hersteller hersteller) {
		this.hersteller = hersteller;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

}
