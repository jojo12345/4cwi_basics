package at.huber.cars;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

//import at.huber.remotes.Device;

public class Owner {
	private String fName;
	private String LName;
	private LocalDate birthday;
	private List<Car> cars;

	public Owner(String fName, String lName, LocalDate birthday) {
		super();
		this.fName = fName;
		LName = lName;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}
	
	public int getTotalCarValue() {
		int number = 0;
		for(Car car : this.cars) {
			number += car.getBasispreis();
		}
		return number;
	}

	public int getAge() {
		LocalDate date = LocalDate.now();
		
		return this.birthday.until(date).getYears();
	}
	
	public void addCar(Car car) {
		//F�gt der Liste cars einen neuen Eintrag hinzu.
		this.cars.add(car);
	}
	
	

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getLName() {
		return LName;
	}

	public void setLName(String lName) {
		LName = lName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	

}
