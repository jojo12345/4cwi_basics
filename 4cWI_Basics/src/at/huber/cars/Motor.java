package at.huber.cars;
public class Motor {
	private String treibstoff;
	private int leistung;


	public Motor(String treibstoff, int leistung) {
		super();
		this.treibstoff = treibstoff;
		this.leistung = leistung;

	}



	public String getTreibstoff() {
		return treibstoff;
	}

	public void setTreibstoff(String treibstoff) {
		this.treibstoff = treibstoff;
	}

	public int getLeistung() {
		return leistung;
	}

	public void setLeistung(int leistung) {
		this.leistung = leistung;
	}

}
