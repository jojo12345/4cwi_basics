package at.huber.cars;

import java.time.LocalDate;

public class CarMain {
	// Kein Constructor, weil hier nichts definiert wird, nur zusammengebaut.

	public static void main(String[] args) {
		// Alle Klassen die in einer Klasse vorkommend, die wir verwenden m�ssen auch
		// ihre Variablen haben.
		Motor m1 = new Motor("diesel", 100);
		Hersteller h1 = new Hersteller("audi", "DE", 10);
		Car car1 = new Car("red", "150", 20000, 5, m1, h1, 50000);
		
		Owner o1 = new Owner("Max","Mustermann", LocalDate.of(2000, 5, 10));
		o1.addCar(car1);
		
		System.out.println(o1.getAge());
		System.out.println(o1.getTotalCarValue());

		System.out.println(car1.getCorrectedComsumption());

	}

}
